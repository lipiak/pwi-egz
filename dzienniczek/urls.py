
from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView
from django.views.generic import ListView

from models import Wpis
import views

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^dodaj/', views.dodaj, name='dodaj'),
        url(r'^add/', views.add, name='add'),
        url(r'^login/', views.login, name='login'),
        url(r'^doLogin/', views.doLogin, name='doLogin'),
        url(r'^preferences/', views.preferences, name='preferences'),
        url(r'^logout/', views.logout, name='logout'),
        url(r'^register/', views.register, name='register'),
        url(r'^doRegister/', views.doRegister, name='doRegister'),
        url(r'^komunikat/(?P<typ>\w+)/(?P<message>\w+)/$', views.komunikat, name='komunikat'),
        url(r'^filter/', views.filter, name='filter'),
        url(r'^doFilter/', views.doFilter, name='doFilter'),
        url(r'^showuserposts/(?P<tag>[a-zA-Z#0-9]+)/$', views.showUserPosts, name='showUserPosts'),
        url(r'^admin/', include(admin.site.urls)),
        url(r'^activate/(?P<token>\w+)/', views.activate, name='activateUser'),
        url(r'^edit/(?P<id>\w+)/$', views.edit, name='editPost'),
        url(r'^doEdit/', views.doEdit, name='doEdit'),
        url(r'^showgrades/(?P<id>\w+)/$', views.showgrades, name='showGrades'),
        url(r'^downloadgrades/(?P<id>\w+)/$', views.dwnlzip, name='dwnlZip'),
        url(r'^canvasLogin/', views.canvasLogin, name='canvasLogin'),
        url(r'^lengthenSession/', views.lenghtenSession, name='lengthenSession'),

)