# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils import timezone

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.mail import send_mail

from models import Wpis
from models import User
from models import Tag
from models import Grupa
from models import CanvasUser
from django.core.cache import cache



import memoryZip

import getpass
import requests
from bs4 import BeautifulSoup

import hashlib



class DodajForm(forms.Form):
    wpis = forms.CharField(max_length=200, min_length=10)
    tagi = forms.CharField(max_length=500)

class LoginForm(forms.Form):
    login = forms.CharField(max_length=100)
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    login = forms.CharField(max_length=100)
    email = forms.EmailField()
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)
    powtorz_haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class FilterForm(forms.Form):
    by_tag = forms.CharField(max_length=100)

class EditForm(forms.Form):
    tresc = forms.CharField(max_length=500)

class CanvasForm(forms.Form):
    login = forms.EmailField()
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)


class PreferencesForm(forms.Form):
    haslo_canvas = forms.CharField(max_length=32, widget=forms.PasswordInput)
    powtorz_haslo_canvas = forms.CharField(max_length=32, widget=forms.PasswordInput)

class WpisTagi(object):

    def __init__(self, tresc, autor, data, tagi):
        self.tresc = tresc
        self.autor = autor
        self.data = data
        self.tagi = tagi


def preferences(request):
    if request.method == "GET":
        form = PreferencesForm()
        return render(request, 'dzienniczek/preferences.html', {
        'forma': form,
        'zalogowany': request.session['zalogowany']})
    else:
        forma = PreferencesForm(request.POST)
        if forma.is_valid():
            hcanvas = forma.cleaned_data['haslo_canvas']
            hcanvas2 = forma.cleaned_data['powtorz_haslo_canvas']
            username = request.session['user']
            u = User.objects.filter(login=username)[0]
            if hcanvas == hcanvas2:
                u.canvas_password = hcanvas
                u.save()
                return HttpResponseRedirect(reverse('komunikat', args=('ok', 'canvas_set',)))



def dodaj(request):
    form = DodajForm() # An unbound form
    return render(request, 'dzienniczek/dodawanie.html', {
        'forma': form,
        'zalogowany': request.session['zalogowany']})


def canvasLogin(request):
    if request.method == 'POST':
        forma = CanvasForm(request.POST)
        if forma.is_valid():
            canvaslogin = forma.cleaned_data['login']
            canvaspassword = forma.cleaned_data['haslo']
            u = CanvasUser(login=canvaslogin, password=canvaspassword, session_time=timezone.now())
            u.save()
            request.session['canvasuser'] = canvaslogin
            return HttpResponseRedirect(reverse('komunikat', args=('ok', 'utworzono_canvas',)))
    else:
        form = CanvasForm()
        return render(request, 'dzienniczek/canvaslogin.html', {
        'forma': form, })

def lenghtenSession(request):

    if 'canvasuser' in request.session:
        if request.session['canvasuser'] is not None:
            u = CanvasUser.objects.filter(login=request.session['canvasuser'])[0]
            u.session_time = timezone.now()
            u.save()
            return HttpResponseRedirect(reverse('komunikat', args=('ok', 'sesja_przedluzona',)))

        else:
            return HttpResponseRedirect(reverse('komunikat', args=('blad','wrong', )))
    else:
        return HttpResponseRedirect(reverse('komunikat', args=('blad','wrong', )))




def login(request):
    forma = LoginForm()
    return render(request, 'dzienniczek/login.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def register(request):
    forma = RegisterForm()
    return render(request, 'dzienniczek/register.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def doLogin(request):
    forma = LoginForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            haslo = forma.cleaned_data['haslo']
            u = User.objects.filter(login=username, haslo=haslo)


            if u.count() is 1:
                if u[0].aktywowany is False:
                    request.session['zalogowany'] = 'False'
                    request.session['user'] = None
                    return HttpResponseRedirect(reverse('komunikat', args=('blad', 'not_activated',)))

                request.session['zalogowany'] = 'True'
                request.session['user'] = username
                return HttpResponseRedirect(reverse('komunikat', args=('ok', 'login_ok',)))
            else:
                request.session['zalogowany'] = 'False'
                request.session['user'] = None
                return HttpResponseRedirect(reverse('komunikat', args=('blad','wrong', )))

    else:
        if request.session['zalogowany'] == 'True':
            return HttpResponseRedirect(reverse('komunikat'))
        else:
            return HttpResponseRedirect(reverse('login'))

def doRegister(request):
    forma = RegisterForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            email = forma.cleaned_data['email']
            haslo = forma.cleaned_data['haslo']
            nhaslo = forma.cleaned_data['powtorz_haslo']
            ilosc = User.objects.filter(login=username).count()
            if ilosc >= 1:
                return HttpResponseRedirect(reverse('komunikat', args=('blad', 'user_exists',)))
            else:
                if not haslo == nhaslo:
                    return HttpResponseRedirect(reverse('komunikat', args=('blad', 'pwd_not_match',)))

                token = hashlib.md5(username + email).hexdigest()

                rev = reverse('activateUser', args=(token,))

                msg = unicode.format(u'Aby aktywowac konto, kliknij w link: http://limitless-bastion-2120.herokuapp.com{0}',
                                     rev)
                send_mail('Aktywacja konta na pr0blogu', msg, 'noreply@pr0blog.pl', [email, ], fail_silently=False)

                u = User(login=username, haslo=haslo, email=email, tokenAktywacyjny=token)
                u.save()



                return HttpResponseRedirect(reverse('komunikat', args=('ok', 'register_ok',)))

    else:
        return HttpResponseRedirect(reverse('register'))

def komunikat(request, typ, message):
    tresc = ''
    if message == 'login_ok':
        tresc = 'Zalogowano pomyslnie'
    elif message == 'logout_ok':
        tresc = 'Wylogowano pomyslnie'
    elif message == 'canvas_set':
        tresc = 'Zapisano haslo canvas'
    elif message == 'not_logged_in':
        tresc = 'Nie jestes zalogowany'
    elif message == 'wpis_ok':
        tresc = 'Twoj wpis dodano pomyslnie'
    elif message == 'user_exists':
        tresc = 'Taki uzytkownik juz istnieje'
    elif message == 'pwd_not_match':
        tresc = 'Podane hasla nie sa identyczne'
    elif message == 'register_ok':
        tresc = 'Rejestracja poprawna. E-mail aktywacyjny zostal wyslany na podany adres.'
    elif message == 'not_activated':
        tresc = 'Konto nie jest aktywne'
    elif message == 'activated':
        tresc = 'Twoje konto zostalo aktywowane. Mozesz sie zalogowac.'
    elif message == 'edit_ok':
        tresc = 'Edycja postu przebiegla pomyslnie.'
    elif message == 'cant_edit':
        tresc = 'Musi minac conajmniej 10 minut od ostatniej edycji lub dodania wpisu'
    elif message == 'sesja_wygasla':
        tresc = 'Twoja sesja wygasla'
    elif message == 'utworzono_canvas':
        tresc = 'Sesja canvas zostala utworzona'
    elif message == 'sesja_przedluzona':
        tresc = 'Przedluzono sesje'
    else:
        tresc = 'Cos chyba kombinujesz'

    if typ == 'ok' or typ == 'blad':
        return render(request, 'dzienniczek/komunikat.html',
            {
                'typ': typ,
                'tresc': tresc,
                'zalogowany': request.session['zalogowany'],
            })
    else:
        return render(request, 'dzienniczek/komunikat.html',
            {
                'typ': 'blad',
                'tresc': tresc,
                'zalogowany': request.session['zalogowany'],
            })


def przedmioty(request):

    username = None
    if 'canvasuser' in request.session:
        username = request.session['canvasuser']

    if username is not None:
        u = CanvasUser.objects.filter(login=username)

        if u.count() == 0:
            return []

        u = u[0]

        url = {
            'login': 'https://canvas.instructure.com/login',
            'grades': 'https://canvas.instructure.com/courses/838324/grades',
            'przedmioty': 'https://canvas.instructure.com/courses'
        }

        username = u.login
        password = u.password
        payload = {
            'pseudonym_session[unique_id]': username,
            'pseudonym_session[password]': password
        }

        # Logowanie
        session = requests.session()
        session.post(url['login'], data=payload)
        # Pobranie strony z ocenami
        response = session.get(url['przedmioty'])
        #response.status_code
        #response.headers['content-type']
        # Odczyt strony
        html = response.text
        # Parsowanie strony
        parsed = BeautifulSoup(html, 'html5lib')
        # Scraping
        przedmioty = []
        linki_przedmiotow = []
        #blok = parsed.find('div', {'id': 'content'})

        kursy_li = parsed.find_all('li', {'class': 'active'})

        for element in kursy_li:
            link = element.find_all('a')
            for l in link:
                linki_przedmiotow.append(l['href'])

            kursy_napisy = element.find('span', {'class': 'name ellipsis'})
            for kurs in kursy_napisy:
                przedmioty.append(kurs)

        return (przedmioty, linki_przedmiotow)

    else:
        return []


def dwnlzip(request, id):


    #najpierw sprawdzic sesje
    username = None
    zip = None
    if 'canvasuser' in request.session:
        username = request.session['canvasuser']

    #jeszcze raz pobranie listy przedmiotow



    #datrtregregreg
    if username is not None:
        zip = cache.get(username)
        (prz, li) = przedmioty(request)


        link = "https://canvas.instructure.com" + li[int(id)] + "/grades"
        u = CanvasUser.objects.filter(login=username)

        if u.count() == 0:
            return []

        u = u[0]



        url = {
            'login': 'https://canvas.instructure.com/login',
            'grades': 'https://canvas.instructure.com/courses/838324/grades',
            'przedmioty': 'https://canvas.instructure.com/courses'
        }

        username = u.login
        password = u.password

        payload = {
            'pseudonym_session[unique_id]': username,
            'pseudonym_session[password]': password
        }

        # Logowanie
        session = requests.session()
        session.post(url['login'], data=payload)
        # Pobranie strony z ocenami
        response = session.get(link)
        #response.status_code
        #response.headers['content-type']
        # Odczyt strony
        html = response.text
        # Parsowanie strony
        parsed = BeautifulSoup(html, 'html5lib')

        oceny_tuple = []

        nazwy = []
        kategorie = []
        daty = []
        oceny = []

        #kazdy wiersz tabeli
        wiersze = parsed.find_all('tr', {'class': 'student_assignment assignment_graded editable'})

        #w kazdym wierszu
        for w in wiersze:
            #znalezc header
            header = w.find('th')
            if header is not None:
                a = header.find('a')
                if a is not None:
                    nazwy.append(a.text)
                else:
                    nazwy.append('')

                #w headerze jeszcze jest kategoria zadania
                kat = header.find('div', {'class': 'context'})
                if kat is not None:
                    kategorie.append(kat.text)
                else:
                    kategorie.append('')

            #znalezc termin oddania
            t = w.find('td', {'class': 'due'})
            if t is not None:
                daty.append(t.text.strip())
            else:
                daty.append('')

            #znalezc punkty za zadanie
            pkt = w.find('td', {'class': 'assignment_score'})
            if pkt is not None:
                grade = pkt.find('span', {'class': 'grade'})
                if grade is not None:
                    oceny.append(grade.text[100:].strip())
            else:
                oceny.append('')


        #przygotowanie tupli do posortowania

        if zip is None:
        #generuj zipa
            zip = memoryZip.InMemoryZip()
            txt=""
            for i in range(0, len(nazwy)):
                txt += kategorie[i] + ', ' + nazwy[i] + ', ' + daty[i] + ', ' + oceny[i] + '\n'
            zip.append('oceny.txt', txt.encode('utf-8'))
            cache.set(username,zip)


    response = HttpResponse(mimetype='application/zip')
    response['Content-Disposition'] = 'filename=logs_%s.zip'
    response.write(zip.read())
    return response

def showgrades(request, id):


    #najpierw sprawdzic sesje
    username = None
    if 'canvasuser' in request.session:
        username = request.session['canvasuser']

    #jeszcze raz pobranie listy przedmiotow



    #datrtregregreg
    if username is not None:

        (prz, li) = przedmioty(request)


        link = "https://canvas.instructure.com" + li[int(id)] + "/grades"
        u = CanvasUser.objects.filter(login=username)

        if u.count() == 0:
            return []

        u = u[0]



        url = {
            'login': 'https://canvas.instructure.com/login',
            'grades': 'https://canvas.instructure.com/courses/838324/grades',
            'przedmioty': 'https://canvas.instructure.com/courses'
        }

        username = u.login
        password = u.password

        payload = {
            'pseudonym_session[unique_id]': username,
            'pseudonym_session[password]': password
        }

        # Logowanie
        session = requests.session()
        session.post(url['login'], data=payload)
        # Pobranie strony z ocenami
        response = session.get(link)
        #response.status_code
        #response.headers['content-type']
        # Odczyt strony
        html = response.text
        # Parsowanie strony
        parsed = BeautifulSoup(html, 'html5lib')

        oceny_tuple = []

        nazwy = []
        kategorie = []
        daty = []
        oceny = []

        #kazdy wiersz tabeli
        wiersze = parsed.find_all('tr', {'class': 'student_assignment assignment_graded editable'})

        #w kazdym wierszu
        for w in wiersze:
            #znalezc header
            header = w.find('th')
            if header is not None:
                a = header.find('a')
                if a is not None:
                    nazwy.append(a.text)
                else:
                    nazwy.append('')

                #w headerze jeszcze jest kategoria zadania
                kat = header.find('div', {'class': 'context'})
                if kat is not None:
                    kategorie.append(kat.text)
                else:
                    kategorie.append('')

            #znalezc termin oddania
            t = w.find('td', {'class': 'due'})
            if t is not None:
                daty.append(t.text.strip())
            else:
                daty.append('')

            #znalezc punkty za zadanie
            pkt = w.find('td', {'class': 'assignment_score'})
            if pkt is not None:
                grade = pkt.find('span', {'class': 'grade'})
                if grade is not None:
                    oceny.append(grade.text[100:].strip())
            else:
                oceny.append('')

        #przygotowanie tupli do posortowania


        for i in range(0, len(nazwy)):
            oceny_tuple.append((kategorie[i], nazwy[i], daty[i], oceny[i]))

        #sortowanie tupla
        sortowany = sorted(oceny_tuple, key=lambda wpis: wpis[0])

        user_time = u.session_time
        current = timezone.now()
        dt = current - user_time



    return render(request, 'dzienniczek/grades.html', {
        'zalogowany': request.session['zalogowany'],
        'user': request.session['user'],
        'wszystkie_wpisy': sortowany,
        'czas': 120 - dt.seconds,
        'id': id,
    })


def index(request):
    if not request.session.get('zalogowany'):
        request.session['zalogowany'] = 'False'
        request.session['user'] = None

    #sprawdzanie czy zalogowany uzytkownik jest moderatorem
    ile = User.objects.filter(login=request.session['user'], grupy__nazwa='moderator').count()

    username=None
    if 'canvasuser' in request.session:
        username = request.session['canvasuser']


    if username is not None:
        u = CanvasUser.objects.filter(login=username)[0]
        aktualna = timezone.now()
        usera = u.session_time

        dt = aktualna - usera
        if dt.seconds >= 120:
            del request.session['canvasuser']
            u.delete()
            return HttpResponseRedirect(reverse('komunikat', args=('blad', 'sesja_wygasla',)))

        else:
            return render(request, 'dzienniczek/index.html', {
            'zalogowany': request.session['zalogowany'],
            'user': request.session['user'],
            'wszystkie_wpisy': przedmioty(request),
            'czas': 120 - dt.seconds, })

    return render(request, 'dzienniczek/index.html', {
        'zalogowany': request.session['zalogowany'],
        'user': request.session['user'],
        'moderator': bool(ile),
        'wszystkie_wpisy': przedmioty(request),
    })

def logout(request):
    if request.session['zalogowany'] == 'True':
        request.session['zalogowany'] = 'False'
        request.session['user'] = None
        return HttpResponseRedirect(reverse('komunikat', args=('ok', 'logout_ok',)))
    else:
        return HttpResponseRedirect(reverse('komunikat', args=('blad', 'not_logged_in',)))



def add(request):
    if request.session['zalogowany'] == 'True':
        if request.method == 'POST': # If the form has been submitted...
            # ContactForm was defined in the the previous section
            form = DodajForm(request.POST) # A form bound to the POST data
            if form.is_valid():
                user = request.session['user']
                tresc = form.cleaned_data['wpis']
                tagi = form.cleaned_data['tagi']
                data = timezone.now()
                w = Wpis(autor=user, tresc=tresc, data=data)
                w.save()

                #obrobka tagow
                splitted = unicode(tagi).split(u' ')

                #sprawdzic czy tag jest w bazie, jesli nie to dodac
                for tag in splitted:
                    if Tag.objects.filter(name=tag).count() is 0:
                        t = Tag(name=tag)
                        t.save()
                        w.tagi.add(t)
                    else:
                        t = Tag.objects.filter(name=tag)[0]
                        w.tagi.add(t)

                return HttpResponseRedirect(reverse('komunikat', args=('ok', 'wpis_ok',)))

        return HttpResponseRedirect(reverse('komunikat', args=('blad', 'fdsfds',)))

    else:
        return HttpResponseRedirect(reverse('komunikat', args=('blad', 'not_logged_in',)))

def filter(request):
    forma = FilterForm()
    return render(request, 'dzienniczek/filter.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def doFilter(request):

    if request.method == 'POST':
        forma = FilterForm(request.POST)

        if forma.is_valid():
            tag = forma.cleaned_data['by_tag']
            return HttpResponseRedirect(reverse('showUserPosts', args=(tag,)))

    return HttpResponseRedirect(reverse('index'))

def showUserPosts(request, tag):
    if not request.session.get('zalogowany'):
        request.session['zalogowany'] = 'False'

    #najpierw pobrac wszystkie wpisy, ktore maja dany tag, jesli nie ma tagu to wszystkie
    if tag != '':
        t = Tag.objects.filter(name=tag)
        po_tagach = Wpis.objects.filter(tagi=t)

    else:
        po_tagach = Wpis.objects.all()


    return render(request, 'dzienniczek/index.html', {
        'zalogowany': request.session['zalogowany'],
        'wszystkie_wpisy': po_tagach.order_by('-data'),
        })

def activate(request, token):
    #sprawdz czy nieaktywowany user z takim tokenem jest w bazie
    u = User.objects.filter(aktywowany=False, tokenAktywacyjny=token)

    if u.count() is 1:
        user = u[0]
        user.aktywowany = True
        user.save()
        return HttpResponseRedirect(reverse('komunikat', args=('ok', 'activated',)))

    return HttpResponseRedirect(reverse('komunikat'))

def doEdit(request):

    if request.method == 'POST':
        forma = EditForm(request.POST)

        if forma.is_valid():
            #znajdz wpis o danym id
            id = int(request.session['edit_post'])
            nowa_tresc = forma.cleaned_data['tresc']
            wpis = Wpis.objects.filter(id=id)[0]
            wpis.tresc = nowa_tresc
            wpis.dataModyfikacji = timezone.now()
            wpis.autorModyfikacji = request.session['user']
            wpis.save()
            request.session['edit_post'] = ''
            return HttpResponseRedirect(reverse('komunikat', args=('ok', 'edit_ok',)))

    return HttpResponseRedirect(reverse('komunikat', args=('blad', 'wrong',)))

def edit(request, id):
    #sprawdzic czy zalogowany user jest moderatorem
    ile = User.objects.filter(login=request.session['user'], grupy__nazwa='moderator').count()
    post = Wpis.objects.filter(id=id)[0]

    #jesli jest
    if bool(ile):
        forma = EditForm()
        request.session['edit_post'] = id
        return render(request, 'dzienniczek/edit.html', {
            'forma': forma,
            'zalogowany': request.session['zalogowany']})
    #jesli nie jest
    else:
        if post.dataModyfikacji is not None:
            ostatnia_edycja = post.dataModyfikacji
        else:
            ostatnia_edycja = post.data

        roznica = (timezone.now() - ostatnia_edycja).seconds/60

        if roznica < 10:
            return HttpResponseRedirect(reverse('komunikat', args=('blad', 'cant_edit',)))

        else:
            forma = EditForm()
            request.session['edit_post'] = id
            return render(request, 'dzienniczek/edit.html', {
            'forma': forma,
            'zalogowany': request.session['zalogowany']})
